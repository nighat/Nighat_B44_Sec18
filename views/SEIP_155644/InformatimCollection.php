<?php
require_once("../../vendor/autoload.php");
use App\Message;
if(!isset($_SESSION))session_start();
$msg = Message::message();

echo "<div id='message'><h2 align='center'>Students Grading Result</h2></div>";


?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Information Collection Form</title>
</head>
<body>


<form action="process.php"method="post">
<table border="1" bgcolor="#87ceeb" align="center" width="550">
    <tr><td>Enter Student's Name:</td>
    <td><input type="text"name="name"></td></tr>
    <tr><td>Enter Student's Roll:</td>
    <td><input type="text"name="roll"></td></tr>

    <tr><td>BanglaMark:</td>
    <td><input type="number" step="any" min="0"max="100" name="mark_bangla"></td></tr>
    <tr><td>EnglishMark:</td>
    <td><input type="number" step="any" min="0"max="100" name="mark_english"></td></tr>
    <tr><td>MathMark:</td>
        <td><input type="number" step="any" min="0"max="100" name="mark_math"></td></tr>
    <tr><td colspan="2"><input type="submit"></td></tr>
    </table>

</form>
<script src="../../../resource/bootstrap/js/jquery"></script>

<script>
jQuery(
    function($){
        $('#message').fadeOut(550);
        $('#message').fadeIn(550);
        $('#message').fadeOut(550);
        $('#message').fadeIn(550);
        $('#message').fadeOut(550);

    }
)

</script>