<?php


namespace App;
use PDO, PDOException;

class DataBase
{
    public $DBH;
    public function __construct()

    {
        if(!isset($_SESSION))session_start();
        try{
          $this->DBH = new PDO('mysql:host=localhost;dbname=grading_system_b44', 'root','');
            $this->DBH->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch(PDOException $er){
            echo $er->getMessage();
        }
    }
}